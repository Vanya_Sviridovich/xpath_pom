using System;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Xpath
{
    public class Tests
    {
        private IWebDriver _drive;

        private const string Xpath1 = "//div[starts-with(@class, 'Box')]/descendant::li/following::li[starts-with(@id, 'jump-to')]/a";
        private const string Xpath2 = "//footer//li[contains(@class, 'flex-self-start')]/following-sibling::li/a";
        private const string Xpath3 = "//input[@name='type']/following-sibling::img";
        private const string Xpath4 = "//form/child::password-strength/following-sibling::p/child::a[text()='Terms of Service']";
        private const string Xpath5 = "//summary[normalize-space(text()) = 'Explore']/following-sibling::div//a[contains(@href, 'topics')]";
        private const string Xpath6 = "//*[contains(text(),'Sign up your team')]";
        private const string Xpath7 = "//*[contains(text(),'Read more')]/preceding::h1[text()='SAP']/..//span[text()='Read more']";
        private const string Xpath8 = "//div//img[contains(@alt, 'Google')]/parent::div/img";
        private const string Xpath9 = "//a[contains(@href, 'personal')]";
        private const string Xpath10 = "//button/ancestor::div/auto-check/following-sibling::div/button";
        private const string Xpath11 = "//footer//h2[text()='Product']/../preceding-sibling::div/a";

        [SetUp]
        public void Setup()
        {
            _drive = new ChromeDriver();
            _drive.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2);
            _drive.Manage().Window.Maximize();
            _drive.Navigate().GoToUrl("https://github.com/");
        }

        [Test]
        public void Xpath_1th()
        {
            XpathLocation.GetElement(_drive, "//label/input").SendKeys("WebDriver");
            XpathLocation.GetElement(_drive, Xpath1).Click();
        }

        [Test]
        public void Xpath_2th()
        {
            XpathLocation.GetElement(_drive, Xpath2).Click();
        }

        [Test]
        public void Xpath_3th()
        {
            XpathLocation.GetElement(_drive, Xpath3).Click();
        }

        [Test]
        public void Xpath_4th()
        {
            XpathLocation.GetElement(_drive, Xpath4).Click();
        }

        [Test]
        public void Xpath_5th()
        {
            XpathLocation.GetElement(_drive, "//summary[normalize-space(text()) = 'Explore']/..").Click();
            XpathLocation.GetElement(_drive, Xpath5).Click();
        }

        [Test]
        public void Xpath_6th()
        {
            XpathLocation.GetElement(_drive, Xpath6);
        }

        [Test]
        public void Xpath_7th()
        {
            var element = XpathLocation.GetElement(_drive, Xpath7);
            Console.WriteLine($"Xpath_7th, text - {element.Text}");
        }
        [Test]
        public void Xpath_8th()
        {
            XpathLocation.GetElement(_drive, Xpath8);
        }

        [Test]
        public void Xpath_9th()
        {
            XpathLocation.GetElement(_drive, Xpath9).Click();
            
        }

        [Test]
        public void Xpath_10th()
        {
            XpathLocation.GetElement(_drive, Xpath10).Click();

        }

        [Test]
        public void Xpath_11th()
        {
            XpathLocation.GetElement(_drive, Xpath11).Click();

        }
        [TearDown]
        public void Close()
        {
            Thread.Sleep(TimeSpan.FromSeconds(1));
            _drive.Quit();
        }
    }
}