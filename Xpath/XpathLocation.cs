using System;
using OpenQA.Selenium;

namespace Xpath
{
    public class XpathLocation
    {
        public static IWebElement GetElement(IWebDriver driver, string str)
        {
            try
            {
                return driver.FindElement(By.XPath(str));
            }
            catch (NoSuchElementException e)
            {
                Console.WriteLine("no such element was found");
                throw;
            }
        }
    }
}