using NUnit.Framework;
using OpenQA.Selenium;


namespace POM_PageFactory
{
    public class Test
    {
        private const string ExpectedColor = "#fffa90";
        private const string ExpectedTitel = "Dropped!";

        private bool _flag;

        private IWebDriver _driver;

        [SetUp]
        public void Setup()
        {
            _driver = Driver.GetDriver();
            _driver.Navigate().GoToUrl("https://demoqa.com/");
        }

        [Test]
        public void CheckDraggedElement()
        {
            HomePage homePage = new HomePage(_driver);
            homePage.DroppableItem.Click();

            Droppable droppablePage = new Droppable(_driver);
            droppablePage.DragElement(_driver);

            var color = droppablePage.GetColorOfDroppableElement();

            var title = droppablePage.StaticSquare.Text;

            if (color.Equals(ExpectedColor) && title.Equals(ExpectedTitel))
                _flag = true;

            Assert.IsTrue(_flag);
        }
    }
}