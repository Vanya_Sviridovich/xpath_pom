using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace POM_PageFactory
{
    public class Driver
    {
        private static IWebDriver _driver;

        public Driver()
        {
        }

        public static IWebDriver GetDriver()
        {
            if (_driver == null)
            {
                _driver = new ChromeDriver();
            }

            return _driver;
        }
    }
}