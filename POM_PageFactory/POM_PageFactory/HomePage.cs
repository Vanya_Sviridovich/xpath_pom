using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace POM_PageFactory
{
    public class HomePage
    {
        public HomePage(IWebDriver driver)
        {
            PageFactory.InitElements(driver, this);
        }
        [FindsBy(How = How.XPath, Using = "//a[text()='Sortable']")]
        public IWebElement SortableItem { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[text()='Selectable']")]
        public IWebElement SelectableItem { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[text()='Resizable']")]
        public IWebElement ResizableItem { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[text()='Droppable']")]
        public IWebElement DroppableItem { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[text()='Draggable']")]
        public IWebElement DraggableItem { get; set; }

    }
}