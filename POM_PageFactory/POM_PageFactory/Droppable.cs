using System;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using SeleniumExtras.PageObjects;

namespace POM_PageFactory
{
    public class Droppable
    {
        public Droppable(IWebDriver driver)
        {
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.Id, Using = "draggable")]
        public IWebElement DraggingSquare { get; set; }
        [FindsBy(How = How.Id, Using = "droppable")]
        public IWebElement StaticSquare { get; set; }


        public void DragElement(IWebDriver driver)
        {
            new Actions(driver).DragAndDrop(DraggingSquare, StaticSquare).Build().Perform();
        }

        public string GetColorOfDroppableElement()
        {
            var value = StaticSquare.GetCssValue("background-color");

            var rgba = Regex.Replace(value, @"[^\\,+\\0-9]", "").Split(',').Select(Int32.Parse).ToArray();

            Color color = Color.FromArgb(rgba[3], rgba[0], rgba[1], rgba[2]);

            return color.Name.Replace(color.Name[0], '#');
        }
    }
}